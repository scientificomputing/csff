
# README for CSFF (Carbohydrate Solution Force Field)

## Purpose : 
Parameter set for Carbohydrates in Solution.

## Files :
CSFF.parm
CSFF.top

## Reference :
M. Kuttel and J. W. Brady and K. J. Naidoo. “Carbohydrate Solution Simulations: Producing a Force Field with Experimentally Consistent Primary Alcohol Rotational Frequencies and Populations”, J. Comput. Chem. 2002, 23, 1236-1243.
DOI: 10.1002/jcc.10119 or PMID: 12210149
